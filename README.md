# Python Project Template

A template for Python projects compatible with PyCharm and Gitlab.

- The `.run/` folder contains PyCharm configurations for development 
  tools like pylint, pytest, pytype, flake8, bandit and sphinx.

- `.gitlab-ci.yml` defines a complete pipeline for Gitlab CI with the
  same tools. It can be a reference for shell commands of such tools.
  The pipeline also builds [badges for the Gitlab](#gitlab-project-badges)
  project and publishes the documentation in Gitlab Pages.

- A [Python interpreter](#python-docker-interpreter) can be built 
  with the `Dockerfile`.

- In PyCharm use the View > Tools Windows > TODO to locate points of 
  intervention outside of the `/src` folder.

## Python Docker interpreter

The `Dockerfile` is meant to build an isolated Python interpreter.
Build the image with:

    docker build -t python-project-template .

Run an ephemeral container with:

    docker run --rm -it -v $(pwd):/opt/project python-project-template bash

From inside the container manually run tests with:

    cd /opt/project/src
    pytest tests


## Gitlab project badges

The Gitlab CI pipeline builds a set of custom badges from the latest
commit in master branch. It follows a list of such badges with the
corresponding configuration to be used in General > Settings > Badges.

- Latest Version
  - `https://gitlab.com/%{project_path}`
  - `https://gitlab.com/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/badges/latest_version.svg?job=custom-badges`
- Coverage
  - `https://gitlab.com/%{project_path}`
  - `https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg`
- Pipeline Status
  - `https://gitlab.com/%{project_path}/-/pipelines`
  - `https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg`
- Pylint Score
  - `https://gitlab.com/%{project_path}`
  - `https://gitlab.com/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/badges/pylint.svg?job=custom-badges`
- GPLv3 License
  - `http://perso.crans.org/besson/LICENSE.html`
  - `https://img.shields.io/badge/License-GPLv3-blue.svg`
- pytype
  - `https://gitlab.com/%{project_path}`
  - `https://gitlab.com/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/badges/pytype.svg?job=custom-badges`
- Flake8
  - `https://gitlab.com/%{project_path}`
  - `https://gitlab.com/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/badges/flake8.svg?job=custom-badges`
- Bandit
  - `https://gitlab.com/%{project_path}`
  - `https://gitlab.com/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/badges/bandit.svg?job=custom-badges`  