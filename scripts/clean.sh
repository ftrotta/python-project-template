
rm -r src/.pylint
rm -r src/.pytest_cache
rm -r src/.pytype
rm -r src/.coverage
rm -r docs/_build
rm -r .eggs
rm -r dist
rm -r src/*.egg-info
find . -type d -name '__pycache__' -depth -delete