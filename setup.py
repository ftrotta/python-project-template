from setuptools import setup
from ftrotta.pycolib.setup import (infer_package_info, get_install_requires)

SRC_PATH = 'src/'

name, project_urls, packages = infer_package_info(
    where=SRC_PATH, group='ftrotta', rtfd=False)

with open('README.md', 'r', encoding='utf-8') as fh:
    long_description = fh.read()

install_requires = get_install_requires('requirements/run.txt', 'gitlab')

setup(
    name=name,
    use_scm_version=True,
    project_urls=project_urls,
    author='Francesco Trotta',  # TODO
    description='A collection of tools of common usage in Python',  # TODO
    long_description=long_description,
    long_description_content_type='text/markdown',
    package_dir={'': SRC_PATH},
    packages=packages,
    install_requires=install_requires,
    setup_requires=[
        'setuptools_scm',
    ],
    classifiers=[  # TODO
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
    python_requires='>=3.6',
)
