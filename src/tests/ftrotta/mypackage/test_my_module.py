# pylint: disable=W0621,C0114,C0115,C0116
import pytest
from ftrotta.pycolib.common_tests import (CallableTest, CallableTestConfig)
from ftrotta.pycolib.log import get_configured_root_logger
from ftrotta.mypackage import my_module as mut

_logger = get_configured_root_logger()


class TestAFunction(CallableTest):

    @classmethod
    @pytest.fixture(scope="class")
    def callable_test_config(cls):
        config = CallableTestConfig(
            callable_under_test=(mut.a_function,),  # MIND THE COMMA!
            default_arg_values_for_tests={
                'an_int': 5,
                'a_str': 'pippo'
            },
            wrong_value_lists={
                'an_int': [-1],
                'a_str': [''],
            },
            expected_output=10,
        )
        return config
