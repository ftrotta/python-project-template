# pylint: disable=W0621,C0114,C0115,C0116
import os
import pytest
import numpy as np
from numpy.testing import (assert_array_equal)
from ftrotta.pycolib.common_tests import (
    CallableTest, CallableTestConfig, get_test_path,
    OptionalArg
)
from ftrotta.pycolib.log import get_configured_root_logger

from ftrotta.mypackage import random_seed as mut

_logger = get_configured_root_logger()


def gtp(relative_path):
    return get_test_path(__name__, relative_path)


class TestGenerateFloat(CallableTest):

    @classmethod
    @pytest.fixture(scope="function")
    def callable_test_config(cls):
        instance = mut.RandomArray()
        config = CallableTestConfig(
            callable_under_test=(instance.generate_float, ),
            default_arg_values_for_tests={
                'width': 50,
                'height': 100,
                'seed': OptionalArg(0),
            },
            wrong_value_lists={
                'width': [0, -1],
                'height': [0, -1],
                'seed': []
            },
        )
        return config


def test_generate_float_coherency():
    serialization_fname = gtp('files/generate_float.npy')
    instance = mut.RandomArray()
    current = instance.generate_float(50, 70, 0)
    if not os.path.exists(serialization_fname):
        _logger.debug('No serialization from previous execution found '
                      'for generate_float')
        np.save(serialization_fname, current)
    else:
        previous = np.load(serialization_fname)
        assert_array_equal(current, previous)


class TestGenerateUint8(CallableTest):

    @classmethod
    @pytest.fixture(scope="function")
    def callable_test_config(cls):
        instance = mut.RandomArray()
        config = CallableTestConfig(
            callable_under_test=(instance.generate_uint8, ),
            default_arg_values_for_tests={
                'width': 50,
                'height': 100,
                'seed': OptionalArg(0),
            },
            wrong_value_lists={
                'width': [0, -1],
                'height': [0, -1],
                'seed': []
            },
        )
        return config


def test_generate_uint8_coherency():
    """Coherency with respect to benchmark from previous execution."""

    serialization_fname = gtp('files/generate_uint8.npy')
    instance = mut.RandomArray()
    current = instance.generate_uint8(50, 70, 0)
    if not os.path.exists(serialization_fname):
        _logger.debug('No serialization from previous execution found '
                      'for generate_uint8')
        np.save(serialization_fname, current)
    else:
        previous = np.load(serialization_fname)
        assert_array_equal(current, previous)
