# -*- coding: utf-8 -*-
"""Documentation for the module goes here.

The documentation must adhere to the Google format for docstrings. A
comprehensive example_ for such format could be useful.

.. _example:
    http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html
"""

import logging
from ftrotta.pycolib import input_checks as ics

_logger = logging.getLogger(__name__)


def a_function(an_int: int, a_str: str) -> int:
    """Return something.

    As of `PEP 257 Multi-line Docstrings`_ the summary line prescribes the
    function or method's effect as a command ("Do this", "Return that"),
    not as a description; e.g. don't write "Returns the pathname ...".

    .. _`PEP 257 Multi-line Docstrings`:
        https://www.python.org/dev/peps/pep-0257/#multi-line-docstrings

    Args:
        an_int: An integer input parameter, that should be greater
            than 0. No type specification is needed, as it is
            annotated in the function signature.

        a_str: A string input parameter, that should be non-empty.

    Returns:
        the double of `an_int``.
    """
    ics.check_positiveint(an_int, 'an_int')
    ics.check_nonempty_str(a_str, 'a_str')
    _logger.debug('Important function completed successfully')
    return 2*an_int
