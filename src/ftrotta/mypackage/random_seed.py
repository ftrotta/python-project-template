# -*- coding: utf-8 -*-
""" The module shows to handle the random seed in methods/functions.
"""
import logging
from typing import (Optional)
import numpy as np
from ftrotta.pycolib import input_checks as ics

_logger = logging.getLogger(__name__)


class RandomArray:
    """A sample class that generates random grayscale images.
    """

    @staticmethod
    def generate_float(
            height: int, width: int, seed: Optional[int] = None) -> np.ndarray:
        """Generate a random float array.

        See also :func:`generate_uint8`.

        Args:
            height:
            width:
            seed:

        Returns:
            np.ndarray(float): Here the type is kept to provide further
            info.
        """
        ics.check_positiveint(height, 'height')
        ics.check_positiveint(width, 'width')
        ics.check_noneable_type(seed, 'seed', int)

        if seed is not None:
            np.random.seed(seed)

        return np.random.rand(height, width)

    @staticmethod
    def generate_uint8(
            height: int, width: int, seed: Optional[int] = None) -> np.ndarray:
        """Generate a random np.uint8 array.

        This method shows how to manage the random seed, for the method itself
        and in invoking other random methods.

        Args:
            height:
            width:
            seed:

        Returns:
            np.ndarray(np.uint8): Here the type is kept to provide further
            info.
        """
        ics.check_noneable_type(seed, 'seed', int)

        if seed is not None:
            np.random.seed(seed)

        temp = RandomArray.generate_float(height, width)  # NO SEED HERE!
        # The seed must not be given to this invocation, given
        #  that it as already been set at the beginning of the
        #  function. This is, obviously, no thread safe.

        res = temp * 256
        return res.astype(np.uint8)
