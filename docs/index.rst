Welcome to PythonProject Template
=================================

.. TODO

A template for Python projects.

Please find the repo at https://gitlab.com/ftrotta/python-project-template .

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
